import datetime
import psycopg2
import random
import argparse
import configparser

config = configparser.ConfigParser()
config.read('config.ini', encoding='utf-8')
#######################################################################################################################################################################
connection = psycopg2.connect(
    dbname=config.get("postgres", "dbname"),
    user=config.get("postgres", "user"),
    password=config.get("postgres", "password"))

cursor = connection.cursor()

digits = '0123456789'
letters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
en_letters = 'abcdefghijklmnopqrstuvwxyz'

today = datetime.datetime.today()


def gen_data(arguments):
    if arguments.truncate is not None and int(arguments.truncate) == 1:
        truncate_db()

    else:

        if arguments.ga is not None:
            get_games(arguments.ga)

        if arguments.to is not None:
            get_tournaments(arguments.to)

        if arguments.te is not None:
            get_teams(arguments.te)

        if arguments.ci is not None:
            get_cities(arguments.ci)

        if arguments.ct is not None:
            get_cities_tournaments(arguments.ct)

        if arguments.co is not None:
            get_commentators(arguments.co)

        if arguments.gt is not None:
            get_games_teams(arguments.gt)

        if arguments.op is not None:
            get_org_persons(arguments.op)

        if arguments.pl is not None:
            get_players(arguments.pl)


def truncate_db():
    cursor.execute("truncate table cities restart identity cascade;"
                   "truncate table cities_tournaments restart identity cascade;"
                   "truncate table commentators restart identity cascade;"
                   "truncate table games restart identity cascade;"
                   "truncate table games_teams restart identity cascade;"
                   "truncate table org_persons restart identity cascade;"
                   "truncate table players restart identity cascade;"
                   "truncate table teams restart identity cascade;"
                   "truncate table tournaments restart identity cascade;"
                   )


def get_cities(amount): # OK
    global cities

    cursor.execute('SELECT tournament_id FROM tournaments')
    fk_tournament_id = cursor.fetchall()

    m_tournament = []
    country = []
    city = []
    temp = []
    for i in range(int(amount)):
        m_tournament.append(random.choice(fk_tournament_id)[0])
        country.append(rnd_string(random.randint(5, 25)))
        city.append(rnd_string(random.randint(5, 25)))
        temp.append(tuple((m_tournament[i], country[i], city[i],)))
    cities_rows = tuple(temp)
    query = "INSERT INTO cities  (fk_tournament_id, country, city) VALUES (%s, %s, %s)"
    cursor.executemany(query, cities_rows)
    cursor.execute('SELECT city_id FROM cities')
    cities = cursor.fetchall()


def get_cities_tournaments(amount): # OK
    global cities_tournaments

    cursor.execute('SELECT city_id FROM cities')
    city_id = cursor.fetchall()

    cursor.execute('SELECT tournament_id FROM tournaments')
    tournament_id = cursor.fetchall()

    m_city = []
    m_tournament = []
    temp = []

    for i in range(int(amount)):
        m_city.append(random.choice(city_id)[0])
        m_tournament.append(random.choice(tournament_id)[0])
        temp.append(tuple((m_city[i], m_tournament[i],)))
    cities_tournaments_rows = tuple(temp)
    query = "INSERT INTO cities_tournaments (city_id, tournament_id) VALUES (%s, %s)"
    cursor.executemany(query, cities_tournaments_rows)


def get_commentators(amount): # OK
    global commentators

    cursor.execute('SELECT game_id FROM games')
    game_id = cursor.fetchall()

    m_game = []
    first_name = []
    second_name = []
    country = []
    age = []

    temp = []
    for i in range(int(amount)):
        m_game.append(random.choice(game_id)[0])
        first_name.append(rnd_string(random.randint(10, 30)))
        second_name.append(rnd_string(random.randint(10, 40)))
        country.append(rnd_string(random.randint(5, 40)))
        age.append(random.randint(17, 47))
        temp.append(tuple((m_game[i], first_name[i], second_name[i], country[i], age[i],)))
    commentators_rows = tuple(temp)
    query = "INSERT INTO commentators  (fk_game_id, first_name, second_name, country, age) VALUES (%s, %s, %s, %s, %s)"
    cursor.executemany(query, commentators_rows)
    cursor.execute('SELECT commentator_id FROM commentators ')
    commentators = cursor.fetchall()


def get_games(amount): # OK
    global games
    name = []
    release_date = []
    cybersport_since = []
    temp = []
    for i in range(int(amount)):
        name.append(rnd_string(random.randint(5, 20)))
        release_date.append(rnd_date(2000, 2010))
        cybersport_since.append(rnd_date(2010, 2022))
        temp.append(tuple((name[i], release_date[i], cybersport_since[i],)))
    games_rows = tuple(temp)
    query = "INSERT INTO games (game_name, release_date, cybersport_since) VALUES (%s, %s, %s)"
    cursor.executemany(query, games_rows)
    cursor.execute('SELECT game_id FROM games')
    games = cursor.fetchall()


def get_games_teams(amount): # OK
    global games_teams

    cursor.execute('SELECT team_id FROM teams')
    team_id = cursor.fetchall()

    cursor.execute('SELECT game_id FROM games')
    game_id = cursor.fetchall()

    m_team = []
    m_game = []

    temp = []
    for i in range(int(amount)):
        m_team.append(random.choice(team_id)[0])
        m_game.append(random.choice(game_id)[0])
        temp.append(tuple((m_team[i], m_game[i],)))
    games_teams_rows = tuple(temp)
    query = "INSERT INTO games_teams (team_id, game_id) VALUES (%s, %s)"
    cursor.executemany(query, games_teams_rows)


def get_org_persons(amount): # OK
    global org_persons

    cursor.execute('SELECT team_id FROM teams')
    team_id = cursor.fetchall()

    m_team = []
    first_name = []
    second_name = []
    person_function = []
    temp = []
    for i in range(int(amount)):
        m_team.append(random.choice(team_id)[0])
        first_name.append(rnd_string(random.randint(5, 32)))
        second_name.append(rnd_string(random.randint(5, 32)))
        person_function.append(rnd_string(random.randint(5, 32)))
        temp.append(tuple((m_team[i], first_name[i], second_name[i], person_function[i],)))
    org_persons_rows = tuple(temp)
    query = "INSERT INTO org_persons  (fk_team_id, first_name, second_name, person_function) VALUES (%s, %s, %s, %s)"
    cursor.executemany(query, org_persons_rows)
    cursor.execute('SELECT person_id FROM org_persons ')
    org_persons = cursor.fetchall()


def get_players(amount): # OK
    global players

    cursor.execute('SELECT team_id FROM teams')
    team_id = cursor.fetchall()

    m_team = []
    nickname = []
    first_name = []
    second_name = []
    player_role = []
    age = []

    temp = []
    for i in range(int(amount)):
        m_team.append(random.choice(team_id)[0])
        nickname.append(rnd_string(random.randint(4, 32)))
        first_name.append(rnd_string(random.randint(5, 20)))
        second_name.append(rnd_string(random.randint(5, 20)))
        player_role.append(rnd_string(random.randint(5, 20)))
        age.append(random.randint(16, 40))
        temp.append(tuple((m_team[i], nickname[i], first_name[i], second_name[i], player_role[i], age[i],)))
    players_rows = tuple(temp)
    query = "INSERT INTO players  (fk_team_id, nickname, first_name, second_name, player_role, age) VALUES (%s, %s, " \
            "%s, %s, %s, %s) "
    cursor.executemany(query, players_rows)
    cursor.execute('SELECT player_id FROM players')
    players = cursor.fetchall()


def get_teams(amount): # OK
    global teams

    team_name = []
    foundation_date = []
    country = []
    players_amount = []

    temp = []
    for i in range(int(amount)):
        team_name.append(rnd_string(random.randint(5, 32)))
        foundation_date.append(rnd_date(2018, 2020))
        country.append(rnd_string(random.randint(5, 32)))
        players_amount.append(random.randint(5, 8))
        temp.append(tuple((team_name[i], foundation_date[i], country[i], players_amount[i],)))
    teams_rows = tuple(temp)
    query = "INSERT INTO teams  (team_name, foundation_date, country, players_amount) VALUES (%s, %s, %s, %s)"
    cursor.executemany(query, teams_rows)
    cursor.execute('SELECT team_id FROM teams')
    teams = cursor.fetchall()


def get_tournaments(amount): #OK
    global tournaments

    cursor.execute('SELECT game_id FROM games')
    fk_game_id = cursor.fetchall()

    game_id = []
    tournament_name = []
    tournament_date = []
    prize_fund = []
    is_finished = []

    temp = []
    for i in range(int(amount)):
        game_id.append(random.choice(fk_game_id)[0])
        tournament_name.append(str(rnd_string(random.randint(4, 60))))
        tournament_date.append(str(rnd_date(2020, 2022)))
        prize_fund.append(random.randint(10000, 7000000))
        is_finished.append(rnd_bool())
        temp.append(tuple((game_id[i], tournament_name[i], tournament_date[i], prize_fund[i], is_finished[i],)))
    tournaments_rows = tuple(temp)
    query = "INSERT INTO tournaments  (fk_game_id, tournament_name, tournament_date, prize_fund, is_finished) VALUES " \
            "(%s, %s, %s, %s, %s) "
    cursor.executemany(query, tournaments_rows)
    cursor.execute('SELECT tournament_id FROM tournaments ')
    tournaments = cursor.fetchall()

def rnd_bool():
    num = random.randint(0, 100)
    if num < 50:
        return True
    else:
        return False


def rnd_time():
    hours = random.randint(1, 23)
    minutes = random.randint(0, 59)
    seconds = random.randint(0, 59)
    time = datetime.time(hours, minutes, seconds)
    return time


def rnd_date(start_year, end_year):
    year = random.randint(start_year, end_year)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    date = datetime.date(year, month, day)
    return date

def rnd_string(length):
    str = ''
    for i in range(int(length)):
        str += random.choice(letters)
    return str


if __name__ == '__main__':
    args = argparse.ArgumentParser(description="Details of data generation")
    args.add_argument('--ci', action="store", dest="ci")
    args.add_argument('--ct', action="store", dest="ct")
    args.add_argument('--co', action="store", dest="co")
    args.add_argument('--ga', action="store", dest="ga")
    args.add_argument('--gt', action="store", dest="gt")
    args.add_argument('--op', action="store", dest="op")
    args.add_argument('--pl', action="store", dest="pl")
    args.add_argument('--te', action="store", dest="te")
    args.add_argument('--to', action="store", dest="to")

    args.add_argument('--truncate', action="store", dest="truncate")

    arguments = args.parse_args()
    gen_data(arguments)
    connection.commit()
    connection.close()
