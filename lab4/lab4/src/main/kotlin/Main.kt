

fun main() {
    val iterations = 1000

    for (interaction in (listOf(NonCachingInteraction, CachingInteraction))) {
        val testThreads = listOf(
            GetAllUserNotesThread(iterations, interaction),
            GetNotesThread(iterations, interaction),
            GetUsersThread(iterations, interaction),
            AddUserNotesThread(iterations, interaction),
            ChangeUsernameThread(iterations, interaction),
            ChangeNoteDescriptionThread(iterations, interaction),
            RemoveUserNotesThread(iterations, interaction)
        )

        testThreads.forEach { it.start() }
        testThreads.forEach { it.join() }

        println(interaction::class.java)
        println("Average")
        for (t in testThreads) {
            println("${t::class.java} - ${(t.times.sum() / t.iterations) / 1000000f}")
        }
        println("Max")
        for (t in testThreads) {
            println("${t::class.java} - ${t.times.maxByOrNull { it }!! / 1000000f}")
        }
        println("Min")
        for (t in testThreads) {
            println("${t::class.java} - ${t.times.minByOrNull { it }!! / 1000000f}")
        }
        println()
        println()
    }
}