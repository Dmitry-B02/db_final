sealed interface Key

data class NoteKey(private val id: Long) : Key

data class UserKey(private val id: Long) : Key

object AllUserNotesKey : Key
