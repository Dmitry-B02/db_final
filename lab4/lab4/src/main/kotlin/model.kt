import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.date

/*object UserEntity : Table() {
    val id = long("id").autoIncrement()
    val username = varchar("username", length = 16)

    override val primaryKey = PrimaryKey(id)
}

object Aroma : Table() {
    val id = long("id").autoIncrement()
    val name = varchar("name", length = 32)
    val description = text("description")

    override val primaryKey = PrimaryKey(id)
}

object Taste : Table() {
    val id = long("id").autoIncrement()
    val name = varchar("name", length = 32)
    val description = text("description")

    override val primaryKey = PrimaryKey(id)
}

object Type : Table() {
    val id = long("id").autoIncrement()
    val name = varchar("name", length = 32)
    val description = text("description")

    override val primaryKey = PrimaryKey(id)
}

object Tea : Table() {
    val id = long("id").autoIncrement()
    val name = varchar("name", length = 255)
    val aromaId = long("aroma_id") references Aroma.id
    val tasteId = long("taste_id") references Taste.id
    val typeId = long("type_id") references Type.id
    val description = text("description")

    override val primaryKey = PrimaryKey(id)
}

object Note : Table() {
    val id = long("id").autoIncrement()
    val teaId = long("tea_id") references Tea.id
    val note = text("note")
    val grade = integer("grade")

    override val primaryKey = PrimaryKey(id)
}

object UserNotes : Table() {
    val id = long("id").autoIncrement()
    val userId = long("user_id") references UserEntity.id
    val noteId = long("note_id") references Note.id

    override val primaryKey = PrimaryKey(id)
}*/

object Games: Table() {
    val game_id = long("game_id").autoIncrement()
    val game_name = varchar("game_name", 32)
    val release_date = date("release_date")
    val cybersport_since = date("cybersport_since")

    override val primaryKey = PrimaryKey(game_id)
}

object Teams: Table() {
    val team_id = long("team_id").autoIncrement()
    val team_name = varchar("team_name", 64)
    val foundation_date = date("foundation_date")
    val country = varchar("country", 64)
    val players_amount = short("players_amount")

    override val primaryKey = PrimaryKey(team_id)
}

object Tournaments: Table() {
    val tournament_id = long("tournament_id").autoIncrement()
    val fk_game_id = long("fk_game_id") references Games.game_id
    val tournament_name = text("tournament_name")
    val tournament_date = date("tournament_date")
    val prize_fund = long("prize_fund")
    val is_finished = bool("is_finished")

    override val primaryKey = PrimaryKey(tournament_id)
}

object Commentators: Table() {
    val commentator_id = long("commentator_id").autoIncrement()
    val fk_game_id = long("fk_game_id") references Games.game_id
    val first_name = varchar("first_name", 32)
    val second_name = varchar("second_name", 64)
    val country = varchar("country", 64)
    val age = short("age")

    override val primaryKey = PrimaryKey(commentator_id)
}

object Cities: Table() {
    val city_id = long("city_id").autoIncrement()
    val fk_tournament_id = long("fk_tournament_id") references Tournaments.tournament_id
    val country = varchar("country", 64)
    val city = varchar("city", 64)

    override val primaryKey = PrimaryKey(city_id)
}

object Players: Table() {
    var player_id = long("player_id").autoIncrement()
    val fk_team_id = long("fk_team_id") references Teams.team_id
    val nickname = varchar("nickname", 64)
    val first_name = varchar("first_name", 32)
    val second_name = varchar("second_name", 64)
    val player_role = varchar("player_role", 64)
    val age = short("age")

    override val primaryKey = PrimaryKey(player_id)
}

object Org_Persons: Table() {
    val person_id = long("person_id").autoIncrement()
    val fk_team_id = long("fk_team_id") references Teams.team_id
    val first_name = varchar("first_name", 32)
    val second_name = varchar("second_name", 64)
    val person_function = text("person_function")

    override val primaryKey = PrimaryKey(person_id)
}

object CitiesTournaments: Table() {
    val city_id = long("city_id") references Cities.city_id
    val tournament_id = long("tournament_id") references Tournaments.tournament_id

    init {
        index(true, city_id, tournament_id)
    }
}

object GamesTeams: Table() {
    val team_id = long("team_id") references Teams.team_id
    val game_id = long("game_id") references Games.game_id

    init {
        index(true, team_id, game_id)
    }
}