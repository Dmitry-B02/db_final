import org.jetbrains.exposed.sql.ResultRow

object CachingInteraction : DatabaseInteraction {

    private val cache = LruBasedCache<Key, List<ResultRow>>(128)

    override fun getAllGames() =
        cache.getOrPut(AllUserNotesKey) { NonCachingInteraction.getAllGames() }

    override fun getPlayer(id: Long) =
        cache.getOrPut(NoteKey(id)) { NonCachingInteraction.getPlayer(id) }

    override fun getCommentator(id: Long) =
        cache.getOrPut(UserKey(id)) { NonCachingInteraction.getCommentator(id) }

    override fun addCity(_fk_tournament_id: Long, _country: String, _city: String) {
        cache.remove(AllUserNotesKey)

        NonCachingInteraction.addCity(_fk_tournament_id, _country, _city)
    }

    override fun changePlayerNickname(userId: Long, newNickname: String) {
        cache.remove(UserKey(userId))

        NonCachingInteraction.changePlayerNickname(userId, newNickname)
    }

    override fun changeTeamName(noteId: Long, newTeamName: String) {
        cache.remove(NoteKey(noteId))

        NonCachingInteraction.changeTeamName(noteId, newTeamName)
    }

    override fun removeGame(userNoteId: Long) {
        cache.remove(AllUserNotesKey)

        NonCachingInteraction.removeGame(userNoteId)
    }
}