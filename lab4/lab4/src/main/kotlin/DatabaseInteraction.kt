import org.jetbrains.exposed.sql.ResultRow
import org.w3c.dom.Text

interface DatabaseInteraction {
    fun getAllGames() : List<ResultRow>

    fun getPlayer(id: Long) : List<ResultRow>

    fun getCommentator(id: Long) : List<ResultRow>

    fun addCity(_fk_tournament_id: Long, _country: String, _city: String)

    fun changePlayerNickname(userId: Long, newNickname: String)

    fun changeTeamName(noteId: Long, newTeamName: String)

    fun removeGame(userNoteId: Long)
}