import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.w3c.dom.Text

object NonCachingInteraction : DatabaseInteraction {
    override fun getAllGames() =
        transaction { Games.selectAll().toList() }

    override fun getPlayer(id: Long) =
        transaction { Players.select { Players.player_id eq id }.toList() }

    override fun getCommentator(id: Long) =
        transaction { Commentators.select { Commentators.commentator_id eq id }.toList() }

    override fun addCity(_fk_tournament_id: Long, _country: String, _city: String) {
        transaction {
            Cities.insert {
                it[fk_tournament_id] = _fk_tournament_id
                it[country] = _country
                it[city] = _city
            }
        }
    }

    override fun changePlayerNickname(userId: Long, newNickname: String) {
        transaction {
            Players.update({ Players.player_id eq userId }) {
                it[nickname] = newNickname
            }
        }
    }

    override fun changeTeamName(noteId: Long, newTeamName: String) {
        transaction {
            Teams.update({ Teams.team_id eq noteId }) {
                it[team_name] = newTeamName
            }
        }
    }

    override fun removeGame(userNoteId: Long) {
        transaction {
            Players.deleteWhere { Players.player_id eq userNoteId }
        }
    }
}