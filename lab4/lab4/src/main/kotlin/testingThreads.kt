import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.concurrent.CyclicBarrier

private fun connectToDatabase() {
    fun hikari(): HikariDataSource {
        val config = HikariConfig()
        config.driverClassName = "org.postgresql.Driver"
        config.jdbcUrl = "jdbc:postgresql://localhost:5432/cybersport"
        config.username = "postgres"
        config.password = "1337"
        config.addDataSourceProperty("cachePrepStmts", "false")

        config.validate()
        return HikariDataSource(config)
    }

    Database.connect(hikari())
}

private val cyclicBarrier = CyclicBarrier(7)

abstract class TestThread(val iterations: Int, val interaction: DatabaseInteraction) : Thread() {
    val times = mutableListOf<Long>()
}

class GetAllUserNotesThread(iterations: Int, interaction: DatabaseInteraction) : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        while (cyclicBarrier.numberWaiting < 4) {
            val start = System.nanoTime()
            interaction.getAllGames()
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}

class GetNotesThread(iterations: Int, interaction: DatabaseInteraction) : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        val notesId = transaction { Players.selectAll().map { it[Players.player_id] } }

        while (cyclicBarrier.numberWaiting < 4) {
            val start = System.nanoTime()
            interaction.getPlayer(notesId.random())
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}

class GetUsersThread(iterations: Int, interaction: DatabaseInteraction) : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        val usersId = transaction { Commentators.selectAll().map { it[Commentators.commentator_id] } }

        while (cyclicBarrier.numberWaiting < 4) {
            val start = System.nanoTime()
            interaction.getCommentator(usersId.random())
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}

class AddUserNotesThread(iterations: Int, interaction: DatabaseInteraction)
    : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        val usersId = transaction { Tournaments.selectAll().map { it[Tournaments.tournament_id] } }

        repeat(iterations) {
            val start = System.nanoTime()
            interaction.addCity(usersId.random(), "dsa", "dsads")
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}

class ChangeUsernameThread(iterations: Int, interaction: DatabaseInteraction) : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        val usersId = transaction { Players.selectAll().map { it[Players.player_id] } }

        repeat(iterations) {
            val start = System.nanoTime()
            interaction.changePlayerNickname(usersId.random(), "testuser")
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}

class ChangeNoteDescriptionThread(iterations: Int, interaction: DatabaseInteraction)
    : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        val notesId = transaction { Teams.selectAll().map { it[Teams.team_id] } }

        repeat(iterations) {
            val start = System.nanoTime()
            interaction.changeTeamName(notesId.random(), "testtest")
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}

class RemoveUserNotesThread(iterations: Int, interaction: DatabaseInteraction) : TestThread(iterations, interaction) {
    override fun run() {
        connectToDatabase()

        cyclicBarrier.await()

        val userNotesId = transaction { Players.selectAll().map { it[Players.player_id] } }

        repeat(iterations) {
            val start = System.nanoTime()
            interaction.removeGame(userNotesId.random())
            times.add(System.nanoTime() - start)
        }

        cyclicBarrier.await()
    }
}
