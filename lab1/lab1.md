# DB Cybersport
- список киберспортивных дисциплин (название игры, год выхода игры, год признания как киберспортивной дисциплины)
- список команд в дисциплине (команда, дата основания, страна, количество игроков)
- список игроков в команде (никнейм, имя, фамилия, роль (если есть), возраст)
- список турниров по игре (название, год проведения, проведён или ещё нет, призовой фонд)
- список комментаторов турнира (имя, фамилия, страна, язык)
- список городов, в которых проводятся турниры (страна, город, дата, турнир)
- список неигрового состава команд (имя, фамилия, название команды, должность)
-------
# Полученная в pgAdmin таблица:
![Alt-текст](https://sun9-51.userapi.com/s/v1/if2/E1ehN8cakhzVVDaIg2VL1Ob_pNkPMYqsGO6CXqQtFTfJ4lfYVggqIDqMV8FPFo-HS_8yaDkH4jhMhyLBaTOCDOnT.jpg?size=780x795&quality=96&type=album "")
-----
# Code
```
CREATE TABLE games 
(
	game_id integer PRIMARY KEY,
	game_name varchar(32),
	release_date date,
	cybersport_since smallint
);

CREATE TABLE teams
(
	team_id integer PRIMARY KEY,
	team_name varchar(64),
	foundation_date date,
	country varchar(64),
	players_amount smallint
);

CREATE TABLE tournaments
(
	tournament_id integer PRIMARY KEY,
	fk_game_id integer REFERENCES games(game_id),
	tournament_name text,
	tournament_date date,
	prize_fund numeric,
	is_finished boolean
);

CREATE TABLE commentators
(
	commentator_id integer PRIMARY KEY,
	fk_game_id integer REFERENCES games(game_id),
	first_name varchar(32),
	second_name varchar(64),
	country varchar(64),
	age smallint
);

CREATE TABLE cities 
(
	city_id integer PRIMARY KEY,
	fk_tournament_id integer REFERENCES tournaments(tournament_id),
	country varchar(64),
	city varchar(64)
);

CREATE TABLE players
(
	player_id integer PRIMARY KEY,
	fk_team_id integer REFERENCES teams(team_id),
	nickname varchar(64),
	first_name varchar(32),
	second_name varchar(64),
	player_role varchar(64),
	age smallint
);

CREATE TABLE org_persons 
(
	person_id integer PRIMARY KEY,
	fk_team_id integer REFERENCES teams(team_id),
	first_name varchar(32),
	second_name varchar(64),
	person_function text
);
	
CREATE TABLE CitiesTournaments
(
	city_id integer REFERENCES cities(city_id),
	tournament_id integer REFERENCES tournaments(tournament_id),
	PRIMARY KEY(city_id, tournament_id)
);

CREATE TABLE GamesTeams
(
	team_id integer REFERENCES teams(team_id),
	game_id integer REFERENCES games(game_id),
	PRIMARY KEY(team_id, game_id)
)
```
