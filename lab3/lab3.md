# Code
```
SELECT * FROM games;

SELECT nickname, player_role FROM players
WHERE nickname LIKE 'д_в%';

SELECT nickname, age FROM players
WHERE age BETWEEN 20 AND 22;

SELECT first_name, age FROM players
WHERE age IN (17, 25, 31);

SELECT game_name,
(cybersport_since - release_date) AS days_to_cybersport
FROM games;

SELECT * FROM players 
ORDER BY age DESC, fk_team_id ASC;

SELECT COUNT(*), MAX(age), MIN(age), AVG(age) from players;

SELECT first_name, team_name FROM org_persons
INNER JOIN teams ON fk_team_ id = team_id;

SELECT nickname, team_name, game_name FROM players
INNER JOIN teams ON fk_team_id = team_id
INNER JOIN games_teams ON teams.team_id = games_teams.game_id
INNER JOIN games ON games_teams.game_id = games.game_id;

SELECT players_amount, COUNT(*) FROM teams
WHERE players_amount > 5
GROUP BY players_amount
HAVING COUNT(*) BETWEEN 2500 AND 2600;

SELECT country, COUNT(*) FROM teams
GROUP BY country
HAVING COUNT(*) > 1;


SELECT * FROM commentators WHERE 
fk_game_id = (SELECT(game_id)
FROM games WHERE game_name = 'эжфмй');
======ЯВЛЯЕТСЯ АНАЛОГОМ:==========
SELECT first_name FROM commentators
INNER JOIN games ON fk_game_id = game_id
WHERE game_name = 'эжфмй';

INSERT INTO games(game_name, release_date, cybersport_since)
VALUES('fsd', '01.01.2015', '01.01.2016');

UPDATE teams SET players_amount = 1
WHERE country = 'USA';
SELECT * FROM teams WHERE country = 'USA';

DELETE FROM players 
WHERE age = (SELECT MIN(age) FROM players);

DELETE FROM teams WHERE 
team_id NOT IN (SELECT fk_team_id FROM org_persons);




```
